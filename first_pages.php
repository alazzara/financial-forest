<?php
	session_start();

	require("includes/db_connect.php");

	include "includes/sandbox.php";

	include "includes/login.php";

	include "includes/sign_up.php";

	include "includes/bank_sign_up.php";

	include "header.php";
?>

		<!-- Enter page -->
		<div data-role="page" id="enterScreen" data-url="enterScreen" data-theme="a">
			<div data-role="header">
				<h1></h1>
			</div>
			<div>
				<div class="background_clouds"></div>
			</div>
			<div class="page_body_container">
				<div data-role="content" data-theme="a">
					<div class="logo">
						<img src="themes/images/logo_image.png" id="logo_image" />
						<p id="tagline">Grow with us!</p>
					</div>
					<br />	      
					<?
						if ($_SESSION['signed_up_need_to_login']) {
							unset($_SESSION['signed_up_need_to_login']);
							print"<p class='alert_text'>Now that you have signed up ... Please Log In!</p>";
						}
						if (isset($_POST['login']) && ($logged_in_correctly_check == false)) {
			    			print"<p class='alert_text'>Please fill out the login form correctly!</p>";
			    		}
			    		if (isset($_POST['login']) && ($logged_in_correctly_check == false)) {
			    			print"<p class='alert_text'>You entered in a incorrect email address or password!</p>";
			    		}
			    		if (isset($_POST['login']) && (!preg_match("/^([A-Za-z0-9]+)(@)([A-Za-z0-9]+)(\.)([A-Za-z]+)$/", $_POST['email']))) {
			    			print"<p class='alert_text'>Please enter in a correct email address!</p>";
			    		}
					?>
				    <form action="" method="post" data-ajax="false">
			    		<input type="text" name="email" id="email" placeholder="Email" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<input type="password" name="login_password" id="login_password" placeholder="Password" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<br /><br />
						<input id="login" name="login" type="submit" value="Login" data-role="button" data-theme="a" />
					</form>
				     <div>
				     	<a href="first_pages.php#signupPage" data-role="button" id="signup_button">Sign Up</a>
				     </div>
				</div>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4 class="footer_bar"> </h4>
			</div>
		</div>
		
		<!-- Signup page -->
		<div data-role="page" id="signupPage" data-url="signupPage" data-theme="a">
			<div data-role="header">
				<h1>Financial Forest</h1>
			</div>
			<div>
				<div class="background_clouds"></div>
			</div>
			<div class="page_body_container">
				<div data-role="content" data-theme="a">	 
					<div class="logo">
						<h1>Sign Up</h1>
					</div>
					<br />
					
					

					<form action="" method="post" data-ajax="false">
						<input type="text" name="username" id="username" placeholder="Username" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<input type="text" name="first_name" id="first_name" placeholder="First Name" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<input type="text" name="last_name" id="last_name" placeholder="Last Name" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
			    		<input type="text" name="sign_up_email" id="sign_up_email" placeholder="Email" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<input type="password" name="password" id="password" placeholder="Password" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<input type="password" name="password_confirmation" id="password_confirmation" placeholder="Password Confirmation" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<br />
						<input id="signup" name="signup" type="submit" value="Sign Up" data-role="button" data-theme="a" /><br />
					</form>    
				</div>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>

		<!-- Bank Info page -->
		<div data-role="page" id="bankInfoPage" data-url="bankInfoPage" data-theme="a">
			<div data-role="header">
				<h1>Financial Forest</h1>
			</div>
			<div>
				<div class="background_clouds"></div>
			</div>
			<div class="page_body_container">
				<div data-role="content" data-theme="a">	 
					<div class="logo">
						<h2>We need your banking info!</h2>
						<h4>We promise we'll keep it safe</h4>
					</div>
					<br />
					<!--<?
						if (isset($_POST['signup']) && (empty($_POST['username'])) && (empty($_POST['first_name'])) && (empty($_POST['last_name'])) && (empty($_POST['sign_up_email'])) && (empty($_POST['password'])) && (empty($_POST['password_confirmation']))) {
							$_SESSION['unsuccessful_signup'] = true;
			    			header('location: first_pages.php#signupPage');
			    		}
			    		if ($_SESSION['unsuccessful_signup'] == true) {
			    			print"<p>Please fill out the login form!</p>";
			    			unset($_SESSION['unsuccessful_signup']);
			    		}
					?>-->
					<form action="" method="post" data-ajax="false">
						<input type="text" name="bank_username" id="bank_username" placeholder="Bank Username" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<input type="password" name="bank_password" id="bank_password" placeholder="Bank Password" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
						<br />
						<input id="bank_signup" name="bank_signup" type="submit" value="Sign Up" data-role="button" data-theme="a" /><br />
					</form>    
				</div>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>

<?php 
	include "footer.php";
?>