<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="ui-mobile">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<base href=".">
		<title>Financial Forest</title> 
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1"> 
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0-beta.1/jquery.mobile-1.2.0-beta.1.min.css">
		<link rel="stylesheet" href="themes/financial_forest.min.css">
		<link rel="stylesheet" href="themes/custom.css">
		<script src="./ff_files/jquery-1.7.2.min.js"></script>
		<script src="./ff_files/jquery.mobile-1.2.0-beta.1.min.js"></script>
	</head> 

	<body class="ui-mobile-viewport ui-overlay-a"> 

		<center>