<?
    //Checking if user hit the submit goal button and actually entered information in the fields
	if(isset($_POST['submit_goal']) && (!empty($_POST['goal_total'])) && (!empty($_POST['goal_description']))) {

        //Setting up the user input into variables
		$user_id = $_SESSION['user_id'];
		$goal_type = $_POST['goal_type'];
		$goal_total = $_POST['goal_total'];
		$goal_description = $_POST['goal_description'];
		$goal_date = $_POST['goal_date'];

        //Entering all of those variables into the user_goals table
        $insert_query_goals = "INSERT INTO user_goals(user_id, goal_type, goal_total, goal_description, goal_date)
            VALUES ('$user_id', 
                    '$goal_type', 
                    '$goal_total',
                    '$goal_description',
                    '$goal_date'
                    )";
        $insert_result_goals = $mysqli->query($insert_query_goals);
        if($mysqli->error) {
            print "Insert query failed: ".$mysqli->error;
        }

        // Return all info from table "users"
        $select_result = $mysqli->query("SELECT * FROM user_goals");

        //Setting a variable to check if the user has a goal already set up
        $goal_check = $_SESSION['user_id'];

        //Checking the user_goals table for the unique user id
        while($row = $select_result->fetch_object()) {

            //If the user already has a goal set up don't display the set new goal button
            //Else display the set new goal button
            if ($goal_check == $row->user_id) { 
                $_SESSION['goal_id'] = $row->goal_id;
            } else {
            }
        }
	}
?>