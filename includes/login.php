<?
    // Return all info from table "users"
    $select_result = $mysqli->query("SELECT * FROM users");

    // If there's an error with the return from "users", well, print that mess. 
    if($mysqli->error) {
        print "Select query error!  Message: ".$mysqli->error;
    }

    // Set default session variables
    $_SESSION['logged_in'] = false;
    $_SESSION['user_id'] = null;
    $_SESSION['username'] = null;
    $_SESSION['email'] = null;
    $_SESSION['display_name'] = null;
    $_SESSION['bank_username'] = null;
    $_SESSION['bank_password'] = null;
    $_SESSION['goal_id'] = null;

    // If login information has been passed through POST...
    if(isset($_POST['login']) 
        && (!$_SESSION['logged_in']) 
        && (!empty($_POST['email'])) 
        && (!empty($_POST['login_password']))
        && (preg_match("/^([A-Za-z0-9]+)(@)([A-Za-z0-9]+)(\.)([A-Za-z]+)$/", $_POST['email']))) {
        
        // Fetch "users" information from $select_result
        while($row = $select_result->fetch_object()) {
            // If the email's the same and the password matches...
            /*========================================================
                PS - THIS IS NOT MYSQLI :: NO OOP! (oops, hehe)
            ========================================================*/
            if ((strtolower($_POST['email']) == strtolower($row->email)) 
                && (md5($_POST['login_password']) == ($row->password))) { 

                $_SESSION['logged_in'] = true;
                $_SESSION['user_id'] = $row->user_id;
                $_SESSION['username'] = $row->login;
                $_SESSION['email'] = strtolower($row->email);
                $_SESSION['display_name'] = $row->display_name;
                $_SESSION['bank_username'] = $row->bank_username;
                $_SESSION['bank_password'] = $row->bank_password;

                // Return all info from table "user_goals"
                $select_result = $mysqli->query("SELECT * FROM user_goals");

                //Setting a variable to check if the user has a goal already set up
                $goal_check = $_SESSION['user_id'];

                //Checking the user_goals table for the unique user id
                while($row = $select_result->fetch_object()) {

                    //If the user already has a goal set up don't display the set new goal button
                    //Else display the set new goal button
                    if ($goal_check == $row->user_id) { 
                        $_SESSION['goal_id'] = $row->goal_id;
                    } else {
                    }
                }

                $logged_in_correctly_check = true;
                $does_the_user_exist = true;

            } else {
                $does_the_user_exist = false;
            }
        }

        if ($_SESSION['logged_in']) {
            header('location: main_page.php');
        }
    } else {
        $logged_in_correctly_check = false;
    }
    
?>