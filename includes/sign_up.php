<?  

    //Stopping php from detecting session variables and local variables of the same name which gives us a warning
    ini_set('session.bug_compat_warn', 0);
    ini_set('session.bug_compat_42', 0);

    //Here is all the code for the sign up systems
    if (isset($_POST['signup']) && (!empty($_POST['username'])) && (!empty($_POST['first_name'])) && (!empty($_POST['last_name'])) && (!empty($_POST['sign_up_email'])) && (!empty($_POST['password'])) && (!empty($_POST['password_confirmation']))) {
            
            //Setting the metakey variables for the "user_meta" table
            $first_name_key = "first_name";
            $last_name_key = "last_name";
            $nickname_key = "nickname";

            //Setting variables from user input for the database tables
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $display_name = $first_name." ".$last_name;
            $username = $_POST['username'];
            $_SESSION['new_username'] = $_POST['username'];
            $password = md5($_POST['password_confirmation']);
            $email = $_POST['sign_up_email'];

            //Inserting all of the necessary user variables ito the "users" table
            $insert_query = "INSERT INTO users(login, password, email, date_registered, display_name)
                VALUES ('$username', 
                        '$password', 
                        '$email',
                        NOW(),
                        '$display_name'
                        )";
            $insert_result = $mysqli->query($insert_query);
            if($mysqli->error) {
                print "Insert query failed: ".$mysqli->error;
            }

            //Selecting correct user id to insert into the "user_meta" table
            $select_user_id = $mysqli->query("SELECT * FROM users WHERE login = '$username'");
            if($mysqli->error) {
                print "Select query error!  Message: ".$mysqli->error;
            }

            while($row = $select_user_id->fetch_object()) {
                $signed_up_user_id = $row->user_id;
            }

            //Inserting the users first name into the "user_meta" table
            $insert_query_meta_first_name = "INSERT INTO user_meta(user_id, meta_key, meta_value)
                VALUES ('$signed_up_user_id', 
                        '$first_name_key', 
                        '$first_name'
                        )";
            $insert_result_meta_first_name = $mysqli->query($insert_query_meta_first_name);
            if($mysqli->error) {
                print "Insert query failed: ".$mysqli->error;
            }

            //Inserting the users last name into the "user_meta" table
            $insert_query_meta_last_name = "INSERT INTO user_meta(user_id, meta_key, meta_value)
                VALUES ('$signed_up_user_id', 
                        '$last_name_key', 
                        '$last_name'
                        )";
            $insert_result_meta_last_name = $mysqli->query($insert_query_meta_last_name);
            if($mysqli->error) {
                print "Insert query failed: ".$mysqli->error;
            }

            //Inserting the users nickname into the "user_meta" table
            $insert_query_meta_nickname = "INSERT INTO user_meta(user_id, meta_key, meta_value)
                VALUES ('$signed_up_user_id', 
                        '$nickname_key', 
                        '$username'
                        )";
            $insert_result_meta_nickname = $mysqli->query($insert_query_meta_nickname);
            if($mysqli->error) {
                print "Insert query failed: ".$mysqli->error;
            }

            header('location: first_pages.php#bankInfoPage');
    }
?>