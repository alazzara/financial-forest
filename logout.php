<?
    session_start();

    if(isset($_SESSION['logged_in'])) {
        unset($_SESSION['logged_in']);
    }
    
    if(isset($_SESSION['user_id'])) {
        unset($_SESSION['user_id']);
    }
    
   	if(isset($_SESSION['username'])) {
        unset($_SESSION['username']);
    }

    if(isset($_SESSION['email'])) {
        unset($_SESSION['email']);
    }
    
    if(isset($_SESSION['display_name'])) {
        unset($_SESSION['display_name']);
    }
    
    if(isset($_SESSION['bank_username'])) {
        unset($_SESSION['bank_username']);
    }

    if(isset($_SESSION['bank_password'])) {
        unset($_SESSION['bank_password']);
    }
    
    if(isset($_SESSION['goal_id'])) {
        unset($_SESSION['goal_id']);
    }
    
    header("Location: first_pages.php");
?>