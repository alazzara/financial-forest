-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2012 at 03:29 PM
-- Server version: 5.1.65
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jaffyesc_portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `goals_progress`
--

CREATE TABLE IF NOT EXISTS `goals_progress` (
  `progress_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `goal_id` bigint(20) unsigned NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `amount` float NOT NULL,
  `percent` float NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`progress_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `parking`
--

CREATE TABLE IF NOT EXISTS `parking` (
  `Parking_Garage` varchar(50) NOT NULL,
  `Map_Image` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parking`
--

INSERT INTO `parking` (`Parking_Garage`, `Map_Image`) VALUES
('Parking Garage C', 'parking_garage_c.png'),
('Parking Garage D', 'parking_garage_d.png'),
('Parking Garage A', 'parking_garage_a.png'),
('Parking Garage B', 'parking_garage_b.png'),
('Parking Garage F', 'parking_garage_f.png'),
('Parking Garage H', 'parking_garage_h.png'),
('Parking Garage I', 'parking_garage_i.png');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `Product_ID` varchar(10) NOT NULL,
  `Product_Name` varchar(100) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Category` varchar(20) NOT NULL,
  `SKU` varchar(10) NOT NULL,
  `Stock` varchar(20) NOT NULL,
  `Cost` varchar(20) NOT NULL,
  `Price` varchar(20) NOT NULL,
  `Sale_Price` varchar(10) NOT NULL,
  `Product_Image` varchar(500) NOT NULL,
  `Featured_Product` varchar(100) NOT NULL,
  `New_Release` varchar(100) NOT NULL,
  `Coming_Soon` varchar(100) NOT NULL,
  `Artist` varchar(100) NOT NULL,
  `Brand` varchar(100) NOT NULL,
  `Rating` int(10) NOT NULL,
  `kewords` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Product_ID`, `Product_Name`, `Description`, `Category`, `SKU`, `Stock`, `Cost`, `Price`, `Sale_Price`, `Product_Image`, `Featured_Product`, `New_Release`, `Coming_Soon`, `Artist`, `Brand`, `Rating`, `kewords`) VALUES
('KD-0001', 'DJ Lance Rock 3" Dunny', 'Get your sillies out with this 3-inch Dunny of Kidrobot''s pal DJ Lance Rock! from Yo Gabba Gabba! The number one DJ in Gabba Land brings the beats with his boom box and trademark tall furry hat. ', 'Vinyl Toys', 'HFO-0004', '55', '$5.99', '$9.99', '', 'dj_lance_rock_product_image.png', '', '', '', 'Kidrobot', 'Kidrobot', 3, 'DJ Lance Rock Dunny Kidrobot Sillies Vinyl Toys'),
('LC-0002', 'Blood &amp; Fuzz 8 inch Dunny', 'Based on Luke''s popular Dunny 2009 design, Blood &amp; Fuzz ups the bloody ante with a severed Dunny head accessory which carries on his ''severed head'' tradition.', 'Vinyl Toys', 'HFO-0003', '2', '$29.99', '$59.99', '', 'blood_and_fuzz_dunny_product_image.png', '', '', '', 'Luke Chueh', 'Kidrobot', 3, 'Blood Fuzz Dunny Luke Chueh Severed Head Kidrobot Vinyl Toys'),
('LC-0001', 'Black in White', 'Based on the original painting, Black in White by Luke Chueh and sculpted by Dave Pressler. Comes with extra fur head that you can swap out with the bare one and a really sharp knife.', 'Vinyl Toys', 'HFO-0001', '5', '$75.99', '$199.99', '', 'black_in_white_product_image.png', '', '', '', 'Luke Chueh', 'Munky King', 5, 'Black White Luke Chueh Dave Pressler Munky King Knife Vinyl Toys'),
('MS-0001', 'Manny Pacquiao', '7 time world boxing champion Manny "Pac-Man" Pacquiao has a vinyl toy! It is freaking amazing!', 'Vinyl Toys', 'HFO-0002', '10', '$49.99', '$99.99', '', 'manny_pacquiao_product_image.png', '', '', '', 'MINDstyle', 'MINDstyle', 5, 'Manny Pacquiao Boxer Pac Man Pac-Man MINDstyle Vinyl Toys Toy'),
('SA-0001', 'Business Ninja Wee Vinyl', 'When Ninjatown is invaded by the evil Mr. Demon and his hotheaded minions, it''s up to Wee Ninja and his 3-inch high-kicking friends to save the day! Enter the fray with the first ever vinyl Ninjatown blind-box mini series featuring 21 warriors for the fate of Shawnimaland!', 'Vinyl Toys', 'HFO-0005', '34', '$5.99', '$7.99', '', 'business_ninja_product_image.png', '', '', '', 'Shawnimals', 'Shawnimals', 4, 'Business Ninja Wee Ninjatown Mr. Demon Shawnimaland Shawnimals Vinyl Toys Toy'),
('SA-0002', 'Mayor Ninja Wee Vinyl', 'When Ninjatown is invaded by the evil Mr. Demon and his hotheaded minions, it''s up to Wee Ninja and his 3-inch high-kicking friends to save the day! Enter the fray with the first ever vinyl Ninjatown blind-box mini series featuring 21 warriors for the fate of Shawnimaland!', 'Vinyl Toys', 'HFO-0006', '20', '$5.99', '$7.99', '', 'mayor_ninja_product_image.png', '', '', '', 'Shawnimals', 'Shawnimals', 4, 'Mayor Ninja Wee Ninjatown Mr. Demon Shawnimaland Shawnimals Vinyl Toys Toy'),
('KD-0002', 'Pity &amp; Gloomy', 'The naive child Pity adopted Gloomy Bear as a cub, thinking he could domesticate the cute little guy.  Unfortunately, that cuddly pink exterior had Pity fooled.  Standing at an imposing 5-inches, Gloomy Bear is thirsty for blood and primed to attack in this latest iteration of Mori Chack''s vicious icon. ', 'Vinyl Toys', 'HFO-0007', '12', '$15.99', '$29.99', '$7.99', 'pity_and_gloomy_product_image.png', 'featured_product.jpg', '', '', 'Mori Chack', 'Kidrobot', 4, 'Pity Gloomy Bear Mori Chack Kidrobot Vinyl Toys Toy'),
('AT-0001', 'King Ken Mini Pink Edition', 'Produced by Amos there are 2 series of these minature Kens.', 'Vinyl Toys', 'HFO-0008', '111', '$5.99', '$7.99', '', 'king_ken_pink_product_image.png', '', '', '', 'James Jarvis', 'Amos', 2, 'King Ken Mini Pink Edition James Jarvis Amos Vinyl Toys Toy'),
('M-0001', 'Iron Man Be@rbrick', 'A Medicom Japanese Import! Medicom keeps rolling with Series 23 of their world-famous Be@rbrick mini-figures! Featuring pop culture''s greatest licenses and the coolest artists, each Be@rbrick stands 2 1/2" tall, with 6 points of articulation.', 'Vinyl Toys', 'HFO-0009', '45', '$5.99', '$7.99', '', 'iron_man_bearbrick_product_image.png', '', '', '', 'Medicom', 'Medicom', 1, 'Iron Man Be@rbrick Bearbrick Bear Brick Medicom Vinyl Toys Toy Japanese Import'),
('L-0001', 'Cup Of Tea: Oolong', 'Direct from the UK - English artist Matt Lunartik Jones presents a decidedly British take on the Art Toy with Lunartik in a Cup of Tea! This first series features four colors in special Collector''s Editions.  Each of these 6.5 vinyl figures comes complete with Cup &amp; Saucer, Tea Stirrer &amp; Sugar Cubes! Freshly packed in a classic window box.', 'Vinyl Toys', 'HFO-0010', '78', '$5.99', '$7.99', '', 'oolong_tea_product_image.png', '', '', '', 'Matt Jones', 'Lunartik', 3, 'Cup Tea Oolong UK Matt Lunartik Jones Collector Edition Vinyl Toys Toy UK England English Import '),
('KD-0001', 'DJ Lance Rock 3" Dunny', 'Get your sillies out with this 3-inch Dunny of Kidrobot''s pal DJ Lance Rock! from Yo Gabba Gabba! The number one DJ in Gabba Land brings the beats with his boom box and trademark tall furry hat. ', 'Vinyl Toys', 'HFO-0004', '55', '$5.99', '$9.99', '', 'dj_lance_rock_product_image.png', '', '', '', 'Kidrobot', 'Kidrobot', 3, 'DJ Lance Rock Dunny Kidrobot Sillies Vinyl Toys'),
('LC-0002', 'Blood &amp; Fuzz 8 inch Dunny', 'Based on Luke''s popular Dunny 2009 design, Blood &amp; Fuzz ups the bloody ante with a severed Dunny head accessory which carries on his ''severed head'' tradition.', 'Vinyl Toys', 'HFO-0003', '2', '$29.99', '$59.99', '', 'blood_and_fuzz_dunny_product_image.png', '', '', '', 'Luke Chueh', 'Kidrobot', 3, 'Blood Fuzz Dunny Luke Chueh Severed Head Kidrobot Vinyl Toys'),
('LC-0001', 'Black in White', 'Based on the original painting, Black in White by Luke Chueh and sculpted by Dave Pressler. Comes with extra fur head that you can swap out with the bare one and a really sharp knife.', 'Vinyl Toys', 'HFO-0001', '5', '$75.99', '$199.99', '', 'black_in_white_product_image.png', '', '', '', 'Luke Chueh', 'Munky King', 5, 'Black White Luke Chueh Dave Pressler Munky King Knife Vinyl Toys'),
('MS-0001', 'Manny Pacquiao', '7 time world boxing champion Manny "Pac-Man" Pacquiao has a vinyl toy! It is freaking amazing!', 'Vinyl Toys', 'HFO-0002', '10', '$49.99', '$99.99', '', 'manny_pacquiao_product_image.png', '', '', '', 'MINDstyle', 'MINDstyle', 5, 'Manny Pacquiao Boxer Pac Man Pac-Man MINDstyle Vinyl Toys Toy'),
('SA-0001', 'Business Ninja Wee Vinyl', 'When Ninjatown is invaded by the evil Mr. Demon and his hotheaded minions, it''s up to Wee Ninja and his 3-inch high-kicking friends to save the day! Enter the fray with the first ever vinyl Ninjatown blind-box mini series featuring 21 warriors for the fate of Shawnimaland!', 'Vinyl Toys', 'HFO-0005', '34', '$5.99', '$7.99', '', 'business_ninja_product_image.png', '', '', '', 'Shawnimals', 'Shawnimals', 4, 'Business Ninja Wee Ninjatown Mr. Demon Shawnimaland Shawnimals Vinyl Toys Toy'),
('SA-0002', 'Mayor Ninja Wee Vinyl', 'When Ninjatown is invaded by the evil Mr. Demon and his hotheaded minions, it''s up to Wee Ninja and his 3-inch high-kicking friends to save the day! Enter the fray with the first ever vinyl Ninjatown blind-box mini series featuring 21 warriors for the fate of Shawnimaland!', 'Vinyl Toys', 'HFO-0006', '20', '$5.99', '$7.99', '', 'mayor_ninja_product_image.png', '', '', '', 'Shawnimals', 'Shawnimals', 4, 'Mayor Ninja Wee Ninjatown Mr. Demon Shawnimaland Shawnimals Vinyl Toys Toy'),
('KD-0002', 'Pity &amp; Gloomy', 'The naive child Pity adopted Gloomy Bear as a cub, thinking he could domesticate the cute little guy.  Unfortunately, that cuddly pink exterior had Pity fooled.  Standing at an imposing 5-inches, Gloomy Bear is thirsty for blood and primed to attack in this latest iteration of Mori Chack''s vicious icon. ', 'Vinyl Toys', 'HFO-0007', '12', '$15.99', '$29.99', '$7.99', 'pity_and_gloomy_product_image.png', 'featured_product.jpg', '', '', 'Mori Chack', 'Kidrobot', 4, 'Pity Gloomy Bear Mori Chack Kidrobot Vinyl Toys Toy'),
('AT-0001', 'King Ken Mini Pink Edition', 'Produced by Amos there are 2 series of these minature Kens.', 'Vinyl Toys', 'HFO-0008', '111', '$5.99', '$7.99', '', 'king_ken_pink_product_image.png', '', '', '', 'James Jarvis', 'Amos', 2, 'King Ken Mini Pink Edition James Jarvis Amos Vinyl Toys Toy'),
('M-0001', 'Iron Man Be@rbrick', 'A Medicom Japanese Import! Medicom keeps rolling with Series 23 of their world-famous Be@rbrick mini-figures! Featuring pop culture''s greatest licenses and the coolest artists, each Be@rbrick stands 2 1/2" tall, with 6 points of articulation.', 'Vinyl Toys', 'HFO-0009', '45', '$5.99', '$7.99', '', 'iron_man_bearbrick_product_image.png', '', '', '', 'Medicom', 'Medicom', 1, 'Iron Man Be@rbrick Bearbrick Bear Brick Medicom Vinyl Toys Toy Japanese Import'),
('L-0001', 'Cup Of Tea: Oolong', 'Direct from the UK - English artist Matt Lunartik Jones presents a decidedly British take on the Art Toy with Lunartik in a Cup of Tea! This first series features four colors in special Collector''s Editions.  Each of these 6.5 vinyl figures comes complete with Cup &amp; Saucer, Tea Stirrer &amp; Sugar Cubes! Freshly packed in a classic window box.', 'Vinyl Toys', 'HFO-0010', '78', '$5.99', '$7.99', '', 'oolong_tea_product_image.png', '', '', '', 'Matt Jones', 'Lunartik', 3, 'Cup Tea Oolong UK Matt Lunartik Jones Collector Edition Vinyl Toys Toy UK England English Import ');

-- --------------------------------------------------------

--
-- Table structure for table `traffic`
--

CREATE TABLE IF NOT EXISTS `traffic` (
  `Traffic_Intersection` varchar(50) NOT NULL,
  `Map_Image` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `traffic`
--

INSERT INTO `traffic` (`Traffic_Intersection`, `Map_Image`) VALUES
('University and Alafaya', 'university_and_alafaya.png'),
('Gemini and Alafaya', 'gemini_and_alafaya.png'),
('Central Florida and Alafaya', 'central_florida_and_alafaya.png'),
('Centaurus and Alafaya', 'centaurus_and_alafaya.png'),
('Research Pkwy and Alafaya', 'research_and_alafaya.png'),
('Libra Dr and Gemini', 'libra_and_gemini.png'),
('McCulloch Rd and Orion Blvd', 'mcculloch_and_orion.png');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `transaction_type` varchar(60) CHARACTER SET utf8 NOT NULL,
  `transaction_amount` float NOT NULL,
  `transaction_status` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `date_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation_key` varchar(60) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  `bank_username` varchar(60) NOT NULL,
  `bank_password` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_login_key` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`, `email`, `date_registered`, `activation_key`, `status`, `display_name`, `bank_username`, `bank_password`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'austintannerhodges@gmail.com', '2012-11-06 15:40:00', '', 0, 'admin', '', ''),
(48, 'karenjcarlson', '08675d7cbc36151855072eb999df4c2f', 'karenjcarlson@gmail.com', '2012-11-18 21:25:54', '', 0, 'Karen Carlson', '', ''),
(47, 'semetik', '402acb1c3e3f37da6e1bb6cacadc315d', 'Sonacitypro@gmail.com', '2012-11-17 22:31:42', '', 0, 'm f', '', ''),
(46, 'Rcfan0902@aol.com', '15b9e46b296cb495868855e3c7dc05a5', 'rcfan0902@aol.com', '2012-11-17 19:18:54', '', 0, 'Greg Zanmiller', '', ''),
(70, 'jescarcha', '5f4dcc3b5aa765d61d8327deb882cf99', 'A1337guitarist@gmail.com', '2012-11-25 16:55:22', '', 0, 'Jaffy Escarcha', 'AppleA', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `user_goals`
--

CREATE TABLE IF NOT EXISTS `user_goals` (
  `goal_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `goal_type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `goal_total` bigint(100) unsigned NOT NULL,
  `goal_description` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `goal_date` date NOT NULL DEFAULT '0000-00-00',
  `goal_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`goal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `user_goals`
--

INSERT INTO `user_goals` (`goal_id`, `user_id`, `goal_type`, `goal_total`, `goal_description`, `goal_date`, `goal_status`) VALUES
(13, 71, 'savings', 500, 'ajsdfauwbgawvio', '2012-11-30', 0),
(14, 46, 'savings', 100, 'Drug money', '2012-12-27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE IF NOT EXISTS `user_meta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=170 ;

--
-- Dumping data for table `user_meta`
--

INSERT INTO `user_meta` (`meta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'first_name', 'Tanner'),
(2, 1, 'last_name', 'Hodges'),
(3, 1, 'nickname', 'T-Ran-A-Sore-Us'),
(103, 48, 'nickname', 'karenjcarlson'),
(102, 48, 'last_name', 'Carlson'),
(101, 48, 'first_name', 'Karen'),
(100, 47, 'nickname', 'semetik'),
(99, 47, 'last_name', 'f'),
(98, 47, 'first_name', 'm'),
(97, 46, 'nickname', 'Rcfan0902@aol.com'),
(96, 46, 'last_name', 'Zanmiller'),
(95, 46, 'first_name', 'Greg'),
(94, 45, 'nickname', 'jescarcha'),
(93, 45, 'last_name', 'Escarcha'),
(92, 45, 'first_name', 'Jaffy'),
(91, 44, 'nickname', 'jescarcha'),
(90, 44, 'last_name', 'Escarcha'),
(89, 44, 'first_name', 'Jaffy'),
(104, 50, 'first_name', 'Jaffy'),
(105, 50, 'last_name', 'Escarcha'),
(106, 50, 'nickname', 'jescarcha'),
(107, 51, 'first_name', 'Jaffy'),
(108, 51, 'last_name', 'Escarcha'),
(109, 51, 'nickname', 'jescarcha'),
(110, 52, 'first_name', 'Jaffy'),
(111, 52, 'last_name', 'Escarcha'),
(112, 52, 'nickname', 'jescarcha'),
(113, 53, 'first_name', 'Jaffy'),
(114, 53, 'last_name', 'Escarcha'),
(115, 53, 'nickname', 'jescarcha'),
(116, 54, 'first_name', 'Jaffy'),
(117, 54, 'last_name', 'Escarcha'),
(118, 54, 'nickname', 'jescarcha'),
(119, 55, 'first_name', 'Jaffy'),
(120, 55, 'last_name', 'Escarcha'),
(121, 55, 'nickname', 'jescarcha'),
(122, 56, 'first_name', 'Jaffy'),
(123, 56, 'last_name', 'Escarcha'),
(124, 56, 'nickname', 'jescarcha'),
(125, 57, 'first_name', 'Jaffy'),
(126, 57, 'last_name', 'Escarcha'),
(127, 57, 'nickname', 'jescarcha'),
(128, 58, 'first_name', 'Jaffy'),
(129, 58, 'last_name', 'Escarcha'),
(130, 58, 'nickname', 'jescarcha'),
(131, 59, 'first_name', 'Jaffy'),
(132, 59, 'last_name', 'Escarcha'),
(133, 59, 'nickname', 'jescarcha'),
(134, 60, 'first_name', 'Jaffy'),
(135, 60, 'last_name', 'Escarcha'),
(136, 60, 'nickname', 'jescarcha'),
(137, 61, 'first_name', 'Jaffy'),
(138, 61, 'last_name', 'Escarcha'),
(139, 61, 'nickname', 'jescarcha'),
(140, 62, 'first_name', 'Jaffy'),
(141, 62, 'last_name', 'Escarcha'),
(142, 62, 'nickname', 'jescarcha'),
(143, 63, 'first_name', 'Jaffy'),
(144, 63, 'last_name', 'Escarcha'),
(145, 63, 'nickname', 'jescarcha'),
(146, 64, 'first_name', 'Jaffy'),
(147, 64, 'last_name', 'Escarcha'),
(148, 64, 'nickname', 'jescarcha'),
(149, 65, 'first_name', 'Jaffy'),
(150, 65, 'last_name', 'Escarcha'),
(151, 65, 'nickname', 'jescarcha'),
(152, 65, 'first_name', 'Jaffy'),
(153, 65, 'last_name', 'Escarcha'),
(154, 65, 'nickname', 'jescarcha'),
(155, 67, 'first_name', 'Jaffy'),
(156, 67, 'last_name', 'Escarcha'),
(157, 67, 'nickname', 'jescarcha'),
(158, 68, 'first_name', 'Jaffy'),
(159, 68, 'last_name', 'Escarcha'),
(160, 68, 'nickname', 'jescarcha'),
(161, 69, 'first_name', 'Jaffy'),
(162, 69, 'last_name', 'Escarcha'),
(163, 69, 'nickname', 'jescarcha'),
(164, 70, 'first_name', 'Jaffy'),
(165, 70, 'last_name', 'Escarcha'),
(166, 70, 'nickname', 'jescarcha'),
(167, 70, 'first_name', 'Jaffy'),
(168, 70, 'last_name', 'Escarcha'),
(169, 70, 'nickname', 'jescarcha');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
