-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2012 at 10:21 PM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jaffyesc_portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `goals_progress`
--

CREATE TABLE `goals_progress` (
  `progress_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `goal_id` bigint(20) unsigned NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `amount` float NOT NULL,
  `percent` float NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`progress_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `transaction_type` varchar(60) NOT NULL,
  `transaction_amount` float NOT NULL,
  `transaction_status` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `date_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation_key` varchar(60) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  `bank_username` varchar(60) NOT NULL,
  `bank_password` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_login_key` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`, `email`, `date_registered`, `activation_key`, `status`, `display_name`, `bank_username`, `bank_password`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'austintannerhodges@gmail.com', '2012-11-06 15:40:00', '', 0, 'admin', '', ''),
(2, 'karenjcarlson', '08675d7cbc36151855072eb999df4c2f', 'karenjcarlson@gmail.com', '2012-11-18 21:25:54', '', 0, 'Karen Carlson', '', ''),
(3, 'semetik', '402acb1c3e3f37da6e1bb6cacadc315d', 'Sonacitypro@gmail.com', '2012-11-17 22:31:42', '', 0, 'm f', '', ''),
(4, 'Rcfan0902@aol.com', '15b9e46b296cb495868855e3c7dc05a5', 'rcfan0902@aol.com', '2012-11-17 19:18:54', '', 0, 'Greg Zanmiller', '', ''),
(5, 'jescarcha', '5f4dcc3b5aa765d61d8327deb882cf99', 'A1337guitarist@gmail.com', '2012-11-25 16:55:22', '', 0, 'Jaffy Escarcha', 'AppleA', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `user_goals`
--

CREATE TABLE `user_goals` (
  `goal_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `goal_type` varchar(10) NOT NULL,
  `goal_total` bigint(100) unsigned NOT NULL,
  `goal_description` varchar(1000) NOT NULL,
  `goal_date` date NOT NULL DEFAULT '0000-00-00',
  `goal_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`goal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `user_meta`
--

INSERT INTO `user_meta` (`meta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'first_name', 'Tanner'),
(2, 1, 'last_name', 'Hodges'),
(3, 1, 'nickname', 'T-Ran-A-Sore-Us'),
(4, 2, 'nickname', 'karenjcarlson'),
(5, 2, 'last_name', 'Carlson'),
(6, 2, 'first_name', 'Karen'),
(7, 3, 'nickname', 'semetik'),
(8, 3, 'last_name', 'f'),
(9, 3, 'first_name', 'm'),
(10, 4, 'nickname', 'Rcfan0902@aol.com'),
(11, 4, 'last_name', 'Zanmiller'),
(12, 4, 'first_name', 'Greg'),
(13, 5, 'first_name', 'Jaffy'),
(14, 5, 'last_name', 'Escarcha'),
(15, 5, 'nickname', 'jescarcha');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
