<?php
	session_start();

	require("includes/db_connect.php");

	include "includes/logged_in_check.php";

	include "includes/goal_set_up.php";

	include "header.php";

	$to = "Rcfan0902@aol.com";
	$subject = 'Your Financial Forest Update';
	$headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
    $headers .= "From: <Financial_Forest@financialforest.com>";

	$message = '
	<!DOCTYPE html>
	<html lang="en-us">
		<head>
			<title>Financial Forest Update</title>

			<style type="text/css">
				body {
					background: #c5e2e2;
					margin-left:40px;
					margin-right:40px;
				}
				#email_logo {
					width:22%;
					height:16%;
				}
				#email_text_body {
					margin-left:20px;
					margin-top:30px;
					margin-bottom:0px;
				}
				#email_footer_bar {
					height:30px;
					background-repeat: repeat-x;
					background-color:#5d3612;
				}
				#email_header_bar {
					height:30px;
					background-repeat: repeat-x;
					background-color:#5d3612;
					margin-bottom:20px;
				}
				#email_trees {
					margin-left:30px;
				}
				#email_clouds {
					margin-left:30px;
				}
			</style>
		</head>
		<body>
			<div id="email_header_bar" background-color="#5d3612" height="30px" width="100%"></div>
			<img src="http://www.jaffyescarcha.com/portfolio_websites/financial_forest/themes/images/logo_image.png" alt="Financial Forest" id="email_logo" />
			<div id="email_text_body">
				<p>Hello User,</p>

				<p>We have noticed that you saved $XXX.XX! Congratulations! You are on your way to making your goal a reality. You only need to save another $XXX.XX to reach your goal. Keep up the great work and make your tree grow even bigger!</p>

				<p>Sincerely,<br />
					The Financial Forest Team <br />
					123 Fake St. Orlando FL, 32816
				</p>
			</div>

			<img src="http://www.jaffyescarcha.com/portfolio_websites/financial_forest/themes/images/background_email.png" alt="Save the trees!" id="email_trees" /><img src="http://www.jaffyescarcha.com/portfolio_websites/financial_forest/themes/images/background_email.png" alt="Save the trees!" />
			<div id="email_footer_bar" background-color="#5d3612" height="30px" width="100%"></div>
		</body>
	</html>
	';

	mail($to, $subject, $message, $headers);

	header("location:main_page.php");
?>