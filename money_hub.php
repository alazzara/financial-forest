<?php
	session_start();

	require("includes/db_connect.php");

	include "includes/logged_in_check.php";

	include "header.php";
?>

		<!-- Money Hub page -->
		<div data-role="page" id="moneyHub" data-url="moneyHub" data-theme="a">
			<div data-role="header">
				<a href="main_page.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Financial Forest</h1>
				<a href="first_pages.php" data-role="button" data-theme="a" rel="external">Log Out</a>
			</div>
			<div data-role="content" data-theme="a">
				<div id="money_hub_top">
				</div>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>
		
		<!-- Money Tree page -->
		<div data-role="page" id="view_tree" data-url="view_tree" data-theme="a">
			<div data-role="header">
				<a href="main_page.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Financial Forest</h1>
				<a href="first_pages.php" data-role="button" data-theme="a" rel="external">Log Out</a>
			</div>
			<div data-role="content" data-theme="a">
				<div id="tree_image">
				</div>
				<div id="bottom_area">
					<div id="roots_image">
					</div>
					<div id="growth_information">
					</div>
				</div>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>
		
		<!-- Income and Expenses page -->
		<div data-role="page" id="set_income_expenses" data-url="set_income_expenses" data-theme="a">
			<div data-role="header">
				<a href="main_page.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Financial Forest</h1>
				<a href="first_pages.php" data-role="button" data-theme="a" rel="external">Log Out</a>
			</div>
			<div data-role="content" data-theme="a">
				<form action="money_hub.php" method="post" data-ajax="false">
					<h2>Monthly Income</h2>
					<label>Amount</label>
		    		<input type="text" name="income_amount" id="income_amount" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
					<h2>Fixed Expenses:</h2>
					<label>Cable Bill</label>
					<input type="text" name="cable_bill" id="cable_bill" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
					<label>Car Insurance</label>
					<input type="text" name="car_insurance" id="car_insurance" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
					<label>Rent/Mortgage</label>
					<input type="text" name="mortgage" id="mortgage" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
					<h2>Variable Expenses</h2>
					<label>Type:</label>
					<select>
						<option value="gasoline">Gasoline</option>
						<option value="entertainment">Entertainment</option>
						<option value="restaurants">Restaurants</option>
						<option value="supermarkets">Supermarkets</option>
						<option value="merchandise">Merchandise</option>
						<option value="automotive">Automotive</option>
					</select>
					<label>Amount:</label>
					<input type="text" name="variable_amount" id="variable_amount" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
					<label>Description:</label>
					<input type="text" name="description" id="description" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
					<br /><br />
					<input id="submit_expenses" name="submit_expenses" type="submit" value="Submit" data-role="button" data-theme="a" />
				</form>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>
<?php 
	include "footer.php";
?>