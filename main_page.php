<?php
	session_start();

	require("includes/db_connect.php");

	include "includes/logged_in_check.php";

	include "includes/goal_set_up.php";

	include "header.php";
?>

		<!-- Enter page -->
		<div data-role="page" id="enterScreen" data-url="enterScreen" data-theme="a">
			<div data-role="header">
				<a href="main_page.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Financial Forest</h1>
				<a href="first_pages.php" data-role="button" data-theme="a" rel="external">Log Out</a>
			</div>
			<div>
				<div class="background_clouds"></div>
			</div>
			<div data-role="content" data-theme="a">
				<div id="money_hub">
					<a href="money_hub.php" id="money_hub_button" data-role="button" rel="external">
						<p id="button_header">Money Hub</p> 
						<p id="tree_image1">Tree Image</p>
						<p id="tree_image2">Tree Image</p>
						<p id="tree_image3">Tree Image</p>
					</a>
				</div>

				<?
					// Return all info from table "users"
    				$select_result = $mysqli->query("SELECT * FROM user_goals");

    				//Setting a variable to check if the user has a goal already set up
    				$goal_check = $_SESSION['user_id'];

    				//Checking the user_goals table for the unique user id
    				while($row = $select_result->fetch_object()) {

    					//If the user already has a goal set up don't display the set new goal button
    					//Else display the set new goal button
			            if ($goal_check == $row->user_id) { 
			            	$goal_flag = true;
			            } else {
			            }
			        }

			        if (!$goal_flag) {
			        	print"<div>";
							print"<a href=\"main_page.php#goals\" id=\"goals_button\" data-role=\"button\" rel=\"external\">Set New Goal</a>";
						print"</div>";
			        }
				?>

				<div>
					<a href="main_page.php#spending_history" id="history_button" data-role="button" rel="external">Notifications</a>
				</div>
				<div>
					<a href="main_page.php#my_forest" id="myForest_button" data-role="button" rel="external">My Forest</a>
				</div>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>

		<!-- Enter page -->
		<div data-role="page" id="my_forest" data-url="my_forest" data-theme="a">
			<div data-role="header">
				<a href="main_page.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Financial Forest</h1>
				<a href="first_pages.php" data-role="button" data-theme="a" rel="external">Log Out</a>
			</div>
			<div>
				<div class="background_clouds"></div>
			</div>
			<div data-role="content" data-theme="a">
				
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>
		
		<!-- Goals page -->
		<div data-role="page" id="goals" data-url="goals" data-theme="a">
			<div data-role="header">
				<a href="main_page.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Financial Forest</h1>
				<a href="first_pages.php" data-role="button" data-theme="a" rel="external">Log Out</a>
			</div>
			<div>
				<div class="background_clouds"></div>
			</div>
			<div data-role="content" data-theme="a">
				<h2>Set New Goal:</h2>
				<form action="main_page.php" method="post" data-ajax="false">
					<label>Select Goal</label>
					<select id="goal_type" name="goal_type">
						<option value="savings">Savings</option>
					</select>
					<label>Set Goal Total:</label>
					<input type="text" name="goal_total" id="goal_total" placeholder="Goal Total" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset">
					<label>Set Goal Description:</label>
		    		<textarea wrap="hard" name="goal_description" id="goal_description" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset"></textarea>
					<label>Set Completion Date:</label>
					<input type="date" name="goal_date" />
					
					<br /><br />
					<input id="submit_goal" name="submit_goal" type="submit" value="Submit Goal" data-role="button" data-theme="a" />
				</form>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>
		
		<!-- History page -->
		<div data-role="page" id="spending_history" data-url="spending_history" data-theme="a">
			<div data-role="header">
				<a href="main_page.php" data-role="button" data-theme="a" rel="external">Home</a>
				<h1>Financial Forest</h1>
				<a href="first_pages.php" data-role="button" data-theme="a" rel="external">Log Out</a>
			</div>
			<div>
				<div class="background_clouds"></div>
			</div>
			<div data-role="content" data-theme="a">
				<h2>Goals History</h2>
				<form action="main_page.php" method="post" data-ajax="false">
					<label>Select Date:</label>
					<input type="date" name="history_date" />
				</form>
				
				<div id="history_area">
				</div>
			</div>

			<div class="pre_footer">
				<div class="background_image"></div>
			</div>
			<div data-role="footer" data-theme="a">
				<h4> </h4>
			</div>
		</div>
<?php 
	include "footer.php";
?>